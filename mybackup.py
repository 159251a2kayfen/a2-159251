import os
import shutil
import hashlib
import pickle



def	GetHash (filename):
#==========================================================================
	"""CreateFileHash (file): create a signature for the specified file
       Returns a tuple containing  values:
          (the pathname of the file, SHA1 hash)
	"""

	f = None
	signature = None
	try:

		f = open(filename, "rb")  # open for reading in binary mode
		hash = hashlib.sha1()
		s = f.read(16384)
		while (s):
			hash.update(s)
			s = f.read(16384)

		hashValue = hash.hexdigest()   
		signature = hashValue    
	except IOError:
		signature = None
	except OSError:
		signature = None
	finally:
		if f:
			f.close()
		return signature

BACKUPARCHIEVE = "H://IWANTDIRECTORY/BACKUPARCHIEVE"
Alternativebackupdir = "H://Destination/backupdir/"
des = "C://Users/fgan/Desktop/des/objects/"
index = "C://Users/fgan/Desktop/des/index/"
src = raw_input("please input the backup directory:\n")


def Dictionary():
	if not os.path.exists(os.path.join(index,'dictionary.pkl')): 
		myDict = {}
	else:
		myDict = pickle.load(open(index+os.sep+'dictionary.pkl', 'rb'))
		
	for src_file in os.listdir(src):
		full_path1 = os.path.join(src,src_file)
		hashdigit = GetHash(full_path1)
		myDict[full_path1] = hashdigit
		print myDict       
	pickle.dump(myDict,open(index+os.sep+'dictionary.pkl', 'wb'))

def Save(src):
	for src_file in os.listdir(src):
		full_path1 = os.path.join(src, src_file)
		hexh1 = GetHash(full_path1)
		
		if os.listdir(des) == []:
			print "following copy"+full_path1
			shutil.copy(full_path1, des)
			print "renaming"+full_path1
			os.rename(os.path.join(des,src_file),os.path.join(des,hexh1))
		else:
			flag=False
			for des_file in os.listdir(des):
				full_path2 = os.path.join(des, des_file)
				hexh2 = GetHash(full_path2)
				if hexh2 == hexh1:                
					print "did this before"+full_path1
					flag=True
					break
			if flag==False:
				print "following copy"+full_path1
				shutil.copy(full_path1, des)
				print "renaming"+full_path1
				os.rename(os.path.join(des,src_file),os.path.join(des,hexh1))
	Dictionary();
	
	